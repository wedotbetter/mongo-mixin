import mongoose  from 'mongoose';
import Promise   from 'bluebird';
import _         from 'lodash';

import { DatabaseConnectionError } from './errors';

let { Schema } = mongoose;

export const databaseSchema = Schema({
	name    : { type : String, required : true, unique : true },
	uri     : { type : String, required : true },
	options : {},
	schemas : { type : String }
}, {
	collection : 'databases'
});

export const Database = class Database {
	constructor(dbDetail, base) {
		let { name, uri, options, schemas } = dbDetail;
		this.base       = base;
		this.name       = name;
		this.uri        = uri;
		this.options    = options;
		this.schemas    = schemas;
		this._schemas   = base.mongoOptions.schemas[schemas];
		this.connection = {};
	}

	connect() {
		return new Promise((resolve, reject) => {

			this.connection = mongoose.createConnection();
			this.importSchemas();

			if (this.uri.split(',').length > 1) {
				this.connection.openSet(this.uri, this.options);
			} else {
				this.connection.open(this.uri, this.options);
			}

			this.connection.on('open', () => {
				resolve(this);
			});

			this.connection.on('error', err => {
				reject(err);
			});

		});
	}

	schemas(schemas) {
		this._schemas = _.merge(this._schemas, schemas);
		return this;
	}

	close() {
		if (!this.connection)
			throw new DatabaseConnectionError('Database is not connected.');
		return this.connection.close();
	}

	importSchemas() {
		let { bases = {}, discriminators = {}, ...others } = this._schemas;
		bases = { ...bases, ...others };
		_.forEach(bases, (baseSchema, baseModelKey) => {

			let model = this.models[baseModelKey];
			if (!model)
				model = this.model(baseModelKey, _.isFunction(baseSchema) ? baseSchema() : baseSchema);

			if (!discriminators[baseModelKey])
				return;

			_.forEach(discriminators[baseModelKey], (discriminatorSchema, discriminatorKey) => {
				let discriminator = this.discriminators ? this.discriminators[discriminatorKey] : null;
				if (!discriminator)
					model.discriminator(discriminatorKey, _.isFunction(discriminatorSchema) ? discriminatorSchema() : discriminatorSchema);
				return;
			});

		});

	}

	model(...args) {
		if (!this.connection)
			throw new DatabaseConnectionError('Database is not connected.');
		return this.connection.model(...args);
	}

	get models() {
		if (!this.connection)
			throw new DatabaseConnectionError('Database is not connected.');
		return this.connection.models;
	}

	get discriminators() {
		if (!this.connection)
			throw new DatabaseConnectionError('Database is not connected.');
		return this.connection.discriminators;
	}

	get readyState() {
		return this.connection.readyState;
	}

	save() {
		return this.base.mongoManager.model('Database').findOneAndUpdate({
			name : this.name
		}, {
			name    : this.name,
			uri     : this.uri,
			options : this.options,
			schemas : this.schemas
		}, {
			new    : true,
			upsert : true
		}).exec();
	}
};
