import { MongoManagerNotReady } from '../errors';
import { READY_STATE, CONNECTED } from '../mixin';

export default (instance, key, descriptor) => {
	let method = descriptor.value;
	descriptor.value = function(...args) {
		if (instance[READY_STATE] !== CONNECTED)
			throw new MongoManagerNotReady('MongoManager is not ready yet');
		method.apply(instance, args);
	};
};
