import custerror from 'custerror';
export const MongoManagerNotReady = custerror('MongoManagerNotReady');
export const NoMongoManager       = custerror('NoMongoManager');
