import mongoose from 'mongoose';
import Promise from 'bluebird';

let _Promise  = Promise;
let _mongoose = mongoose;
_mongoose.Promise = _Promise;

export const setPromise = customPromise => {
	if (!customPromise || !customPromise.resolve || !(customPromise.resolve instanceof Function)) {
		_Promise = Promise;
		_mongoose.Promise = _Promise;
	}
	_Promise = customPromise;
};

export { _Promise as Promise, _mongoose as mongoose};
