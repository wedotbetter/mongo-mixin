import mongoose  from 'mongoose';
import parseArgs from 'args-options';
import Promise   from 'bluebird';
import _         from 'lodash';

import { SCHEMA_INJECTOR, DISCRIMINATOR_INJECTOR } from './mixin';

let { Schema } = mongoose;

const databaseSchema = new Schema({
	name    : { type : String, required : true },
	uri     : { type : String, required : true },
	options : {},
	meta    : {}
}, {
	collection : 'databases'
});

export default class MongoManager {
	constructor(base, configs) {
		this.base    = base;
		this.configs = configs;
	}

	@parseArgs(['uri'])
	connect(configs = {}) {
		return new Promise((resolve, reject) => {
			let managerConnection = mongoose.createConnection();
			try {
				let { uri, options } = { ...this.configs, ...configs };
				if (uri.split(',').length > 1) {
					managerConnection.openSet(uri, options);
				} else {
					managerConnection.open(uri, options);
				}
			} catch(err) {
				return reject(err);
			}
			managerConnection.on('open', () => {
				this.connection = managerConnection;
				this.connection.model('Database', databaseSchema);
				resolve(this.connection);
			});
			managerConnection.on('error', err => {
				reject(err);
			});
		});
	}


	updateDatabase = async (dbName, dbUpdates) => {
		let dbDetail = await this.connection.model('Database').findOne({ name : dbName }).exec();
		_.merge(dbDetail, dbUpdates);
		dbDetail.markModified('options');
		dbDetail.markModified('meta');
		await dbDetail.save();
		return dbDetail;
	}

	importDatabase = async (dbDetail = {}) => {
		let { Database } = this.connection.models;
		let origDb = await Database.findOne({ name : dbDetail.name }).exec();

		if (origDb) {
			_.merge(origDb, dbDetail);
			origDb.markModified('options');
			origDb.markModified('meta');
			await origDb.save();
			return origDb;
		}

		let newDb = new Database(dbDetail);
		await newDb.save();
		return newDb;
	}

	findDatabase = async query => {
		let { Database } = this.connection.models;
		return await Database.find(query).exec();
	}

	connectDatabase = async dbName => {
		let dbDetail = await this.connection.model('Database').findOne({ name : dbName }).exec();

		let db = await new Promise((resolve, reject) => {
			let dbConnection = mongoose.createConnection();
			try {
				let { uri, options } = dbDetail;
				if (uri.split(',').length > 1) {
					dbConnection.openSet(uri, options);
				} else {
					dbConnection.open(uri, options);
				}
			} catch(err) {
				return reject(err);
			}
			dbConnection.on('open', () => {
				resolve(dbConnection);
			});
			dbConnection.on('error', err => {
				reject(err);
			});
		});
		if (!_.isFunction(this.base[SCHEMA_INJECTOR]))
			return db;
		let schemas = this.base[SCHEMA_INJECTOR](dbDetail) || {};
		if (_.isFunction(schemas.then))
			schemas = await schemas;
		for (let modelName in schemas) {
			let schema = schemas[modelName];
			let model = db.model(modelName, schema);
			if (!_.isFunction(this.base[DISCRIMINATOR_INJECTOR]))
				continue;
			let discriminators = this.base[DISCRIMINATOR_INJECTOR](dbDetail, modelName) || {};
			if (_.isFunction(discriminators.then))
				discriminators = await discriminators;
			_.forEach(discriminators, (schema, discriminatorName) => {
				model.discriminator(discriminatorName, schema);
			});
		}
		return db;
	}
}
