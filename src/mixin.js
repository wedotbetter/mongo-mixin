import { Mixin } from 'mixwith';
import parseArgs from 'args-options';
import mongoose  from 'mongoose';
import Promise   from 'bluebird';

mongoose.Promise = Promise;

import MongoManager                             from './manager';
import { NoMongoManager, MongoManagerNotReady } from './errors';
import ready                                    from './decorators/ready';

export const READY_STATE            = Symbol('READY_STATE');
export const CONNECTED              = Symbol('CONNECTED');
export const DATABASES              = Symbol('DATABASES');
export const SCHEMA_INJECTOR        = Symbol('SCHEMA_INJECTOR');
export const DISCRIMINATOR_INJECTOR = Symbol('DISCRIMINATOR_INJECTOR');
export const MONGO_MANAGER          = Symbol('MONGO_MANAGER');

export default Mixin(baseClass => class MongoDBManager extends baseClass {
	constructor(...args) {
		super(...args);
		this[DATABASES] = {};
	}

	setSchemaInjector(injector) {
		this[SCHEMA_INJECTOR] = injector;
		return this;
	}

	setDiscriminatorInjector(injector) {
		this[DISCRIMINATOR_INJECTOR] = injector;
		return this;
	}

    @parseArgs(['uri'])
	setManager(configs) {
		this[MONGO_MANAGER] = new MongoManager(this, configs);
		return this;
	}

	updateDatabase(dbName, dbUpdates) {
		if (this[READY_STATE] !== CONNECTED)
			throw new MongoManagerNotReady('MongoManager is not ready yet');
		return this[MONGO_MANAGER].updateDatabase(dbName, dbUpdates);
	}

	importDatabase (dbDetail) {
		if (this[READY_STATE] !== CONNECTED)
			throw new MongoManagerNotReady('MongoManager is not ready yet');
		return this[MONGO_MANAGER].importDatabase(dbDetail);
	}

	findDatabase = async (query) => {
		if (this[READY_STATE] !== CONNECTED)
			throw new MongoManagerNotReady('MongoManager is not ready yet');
		return await this[MONGO_MANAGER].findDatabase(query);
	}

	connectDatabase = async dbName => {
		if (this[READY_STATE] !== CONNECTED)
			throw new MongoManagerNotReady('MongoManager is not ready yet');
		if (!this[DATABASES][dbName])
			this[DATABASES][dbName] = await this[MONGO_MANAGER].connectDatabase(dbName);
		return this[DATABASES][dbName];
	}

	init = async () => {
		if (!this[MONGO_MANAGER])
			throw new NoMongoManager('No MongoManager defined');
		await this[MONGO_MANAGER].connect();
		this[READY_STATE] = CONNECTED;
	}

});
