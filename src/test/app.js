import { mix }      from 'mixwith';
import MongoDBMixin from '../mixin';
import * as schemas from './schemas';
import _            from 'lodash';

export const AppInherited = class AppInherited extends mix(class {}).with(MongoDBMixin) {
	constructor(manager) {
		super();
		this.setManager(manager)
            .setSchemaInjector(this.getSchemas)
			.setDiscriminatorInjector(this.getDiscriminators);
	}

	getSchemas = async (dbDetail) => {
		let { schemaName, schemaVersion } = dbDetail.meta;
		let _schemas = schemaName == 'admin' ? schemas[schemaName].baseModels : schemas[schemaName][schemaVersion].baseModels;
		let self = this;
		return _.reduce(_schemas, (result, schema, key) => {
			return {
				...result,
				[key] : _.isFunction(schema)
				? schema(self)
				: schema
			};
		}, {});
	}

	getDiscriminators = (dbDetail, model) => {
		let { schemaName, schemaVersion } = dbDetail.meta;
		let _discriminators = schemaName == 'admin' ? schemas[schemaName].discriminators : schemas[schemaName][schemaVersion].discriminators;
		let self = this;
		return schemaName == 'admin' || (schemaName == 'admin' && model == 'Report')
			? _.reduce(_discriminators, (result, schema, key) => {
				return {
					...result,
					[key] : _.isFunction(schema)
					? schema(self)
					: schema
				};
			}, {})
			: {};
	}

	start() {
		return this.init();
	}
};

export const AppComposited = class AppComposited {
	constructor(manager) {
		const Mongo = MongoDBMixin(class {});
		this.mongo = new Mongo();
		this.mongo.setManager(manager)
            .setSchemaInjector(this.getSchemas)
			.setDiscriminatorInjector(this.getDiscriminators);
	}

	getSchemas = (dbDetail) => {
		let { schemaName, schemaVersion } = dbDetail.meta;
		let _schemas = schemaName == 'admin' ? schemas[schemaName].baseModels : schemas[schemaName][schemaVersion].baseModels;
		let self = this;
		return _.reduce(_schemas, (result, schema, key) => {
			return {
				...result,
				[key] : _.isFunction(schema)
				? schema(self)
				: schema
			};
		}, {});
	}

	getDiscriminators = (dbDetail, model) => {
		let { schemaName, schemaVersion } = dbDetail.meta;
		let _discriminators = schemaName == 'admin' ? schemas[schemaName].discriminators : schemas[schemaName][schemaVersion].discriminators;
		let self = this;
		return schemaName == 'admin' || (schemaName == 'admin' && model == 'Report')
			? _.reduce(_discriminators, (result, schema, key) => {
				return {
					...result,
					[key] : _.isFunction(schema)
					? schema(self)
					: schema
				};
			}, {})
			: {};
	}

	start() {
		return this.mongo.init();
	}
};
