import assert from 'assert';

import { AppInherited, AppComposited } from './app';

const manager = {
	uri : 'mongodb://admin:strongpassword@localhost:15101/mongo_manager',
	options : {
		auth : {
			authdb : 'admin'
		}
	}
};

const adminDbDetail = {
	name : 'admin',
	uri : 'mongodb://admin:strongpassword@localhost:15101/test_admin',
	options : {
		auth : {
			authdb : 'admin'
		}
	},
	meta : {
		schemaName : 'admin',
	}
};

const projectOneDbDetail = {
	name : 'test_project_1',
	uri : 'mongodb://admin:strongpassword@localhost:15101/test_project_1',
	options : {
		auth : {
			authdb : 'admin'
		}
	},
	meta : {
		schemaName : 'project',
		schemaVersion : 'v1'
	}
};

const projectTwoDbDetail = {
	name : 'test_project_2',
	uri : 'mongodb://admin:strongpassword@localhost:15102/test_project_2',
	options : {
		auth : {
			authdb : 'admin'
		}
	},
	meta : {
		schemaName : 'project',
		schemaVersion : 'v2'
	}
};

/*global describe it before */

describe('mongo-mixin(inheritance)', function() {
	let app, adminDb, projectOneDb, projectTwoDb;
	before('should configuring app and starting service without error', async function() {
		app = new AppInherited(manager);
		await app.start();
	});

	it('should able to create databases and query created databases', async function() {
		await app.importDatabase(adminDbDetail);
		await app.importDatabase(projectOneDbDetail);
		await app.importDatabase(projectTwoDbDetail);
		let adminDbDetailFromManager = await app.findDatabase({ name : 'admin' });
		let projOneFromManager = await app.findDatabase({ name : 'test_project_1' });
		let projTwoDetailFromManager = await app.findDatabase({ name : 'test_project_2' });

		assert.deepEqual({
			admin      : adminDbDetailFromManager[0].name,
			projectOne : projOneFromManager[0].name,
			projectTwo : projTwoDetailFromManager[0].name
		}, {
			admin      : 'admin',
			projectOne : 'test_project_1',
			projectTwo : 'test_project_2'
		});
	});

	it('should able to connect to specific database with name', async function() {
		adminDb = await app.connectDatabase('admin');
		assert.equal(adminDb.readyState, 1);
	});

	it('should able to insert records', async function() {

		let { Administrator, User } = adminDb.models;
		await (new Administrator({
			username : 'admin'
		})).save();

		await (new User({
			username : 'user'
		})).save();
	});

	it('should able to find records', async function() {
		let { User } = adminDb.models;
		let foundUser = await User.findOne({ username : 'admin' }).exec();

		assert.deepEqual({
			username : foundUser.username,
			isAdmin  : foundUser.isAdmin
		}, {
			username : 'admin',
			isAdmin  : true
		});
	});

	it('should able to connect another database', async function() {
		projectOneDb = await app.connectDatabase('test_project_1');
		assert.equal(projectOneDb.readyState, 1);
		projectTwoDb = await app.connectDatabase('test_project_2');
		assert.equal(projectTwoDb.readyState, 1);
	});

});

describe('mongo-mixin(composited)', function() {
	let app, adminDb, projectOneDb, projectTwoDb;
	before('should configuring app and starting service without error', async function() {
		app = new AppComposited(manager);
		await app.start();
	});
	it('should able to create databases', async function() {
		await app.mongo.importDatabase(adminDbDetail);
		await app.mongo.importDatabase(projectOneDbDetail);
		await app.mongo.importDatabase(projectTwoDbDetail);
		let adminDbDetailFromManager = await app.mongo.findDatabase({ name : 'admin' });
		let projOneFromManager = await app.mongo.findDatabase({ name : 'test_project_1' });
		let projTwoDetailFromManager = await app.mongo.findDatabase({ name : 'test_project_2' });

		assert.deepEqual({
			admin      : adminDbDetailFromManager[0].name,
			projectOne : projOneFromManager[0].name,
			projectTwo : projTwoDetailFromManager[0].name
		}, {
			admin      : 'admin',
			projectOne : 'test_project_1',
			projectTwo : 'test_project_2'
		});
	});

	it('should able to connect to specific database with name', async function() {
		adminDb = await app.mongo.connectDatabase('admin');
		assert.equal(adminDb.readyState, 1);
	});

	it('should able to insert records', async function() {

		let { Administrator, User } = adminDb.models;
		await (new Administrator({
			username : 'admin'
		})).save();

		await (new User({
			username : 'user'
		})).save();
	});

	it('should able to find records', async function() {
		let { User } = adminDb.models;
		let foundUser = await User.findOne({ username : 'admin' }).exec();

		assert.deepEqual({
			username : foundUser.username,
			isAdmin  : foundUser.isAdmin
		}, {
			username : 'admin',
			isAdmin  : true
		});
	});

	it('should able to connect another database', async function() {
		projectOneDb = await app.mongo.connectDatabase('test_project_1');
		assert.equal(projectOneDb.readyState, 1);
		projectTwoDb = await app.mongo.connectDatabase('test_project_2');
		assert.equal(projectTwoDb.readyState, 1);
	});
});
