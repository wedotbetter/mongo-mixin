import mongoose from 'mongoose';

const { Schema } = mongoose;

export default () => new Schema({
	username : String,
	isAdmin  : { type : Boolean, default : true }
}, {
	collection : 'users'
});
