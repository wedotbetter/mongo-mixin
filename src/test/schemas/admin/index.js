import User          from './user';
import Administrator from './administrator';

export default {
	baseModels : { User },
	discriminators : { Administrator }
};
