import mongoose from 'mongoose';

const { Schema } = mongoose;

export default () => new Schema({
	completeAt : Date
}, {
	collection : 'reports'
});
