import Task             from './task';
import Report           from './report';
import CompletionReport from './completionReport';
import FailureReport    from './failureReport';

export const baseModels = {
	Task,
	Report
};

export const discriminators = {
	CompletionReport,
	FailureReport
};
