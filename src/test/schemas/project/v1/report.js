import mongoose from 'mongoose';

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

export default () => new Schema({
	task : { type : ObjectId, ref : 'Task' }
}, {
	collection : 'reports'
});
