import {Promise} from './injectDependencies';

export const connected = function connected() {
	return new Promise((resolve, reject) => {
		this.on('open', () => {
			resolve();
		});
		this.on('error', err => {
			reject(err);
		});
	});
};

export const uppercasefirst = (str) => `${str.substr(0,1).toUpperCase()}${str.substr(1)}`;

export const ifPromise = function(result) {
	return Promise.resolve(result);
};
